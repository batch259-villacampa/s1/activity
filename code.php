<?php

function getFullAddress($country, $city, $province, $specificAdd){
	return "$specificAdd, $city, $province, $country";
}

function getLetterGrade($grade){
	if($grade <= 100 && $grade >= 98){
		return "A+";
	}else if($grade <= 97 && $grade >= 95){
		return "A";
	}else if($grade <= 94 && $grade >= 92){
		return "A-";
	}else if($grade <= 91 && $grade >= 89){
		return "B+";
	}else if($grade <= 88 && $grade >= 86){
		return "B";
	}else if($grade <= 85 && $grade >= 83){
		return "B-";
	}else if($grade <= 82 && $grade >= 80){
		return "C+";
	}else if($grade <= 79 && $grade >= 77){
		return "C";
	}else if($grade <= 76 && $grade >= 75){
		return "C-";
	}else{
		return "F";
	}
}